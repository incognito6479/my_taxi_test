from rest_framework import routers
from mvp.views import ClientVeiwSet, OrderVeiwSet, DriverVeiwSet


router = routers.SimpleRouter()
router.register(r'clients', ClientVeiwSet)
router.register(r'orders', OrderVeiwSet)
router.register(r'drivers', DriverVeiwSet)


urlpatterns = router.urls