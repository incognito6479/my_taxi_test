from django_filters import rest_framework as filters
# from django_filters import RangeFilter
from mvp.models import Client, Order


class ClientFilterSet(filters.FilterSet): 
	class Meta:
		model = Client
		fields = ['id',]


class OrderFilterSet(filters.FilterSet):
	class Meta:
		model = Order
		fields = ['id', 'client']