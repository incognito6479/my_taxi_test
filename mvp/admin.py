from django.contrib import admin
from mvp.models import Client, Order, Driver


@admin.register(Client)
class ClientAdmin(admin.ModelAdmin):
	pass


@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
	pass


@admin.register(Driver)
class DriverAdmin(admin.ModelAdmin):
	pass