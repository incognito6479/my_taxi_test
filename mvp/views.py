from rest_framework import viewsets
from rest_framework.response import Response
from mvp.models import Client, Order, Driver
from mvp.serializers import ClientSerializer, OrderSerializer, DriverSerializer
from mvp.filters import ClientFilterSet, OrderFilterSet
from mvp.helpers import create_order, order_status_update


class ClientVeiwSet(viewsets.ModelViewSet):
	queryset = Client.objects.all()
	serializer_class = ClientSerializer
	# filter_class = ClientFilterSet

	def list(self, request):
		queryset = Client.objects.all()
		client_id = request.GET.get('client_id', 0)
		if (client_id != 0) and (client_id != ''):
			queryset = queryset.filter(id=client_id)
			created_to = request.GET.get('to', 0)
			created_from = request.GET.get('from', 0)
			order = Order.objects.filter(client_id=client_id)
			if (created_from != 0) and (created_to != 0):
				order = order.filter(created__range=[created_from, created_to]) # date format YYYY-MM-DD 2021-05-29
			order_serializer = OrderSerializer(data=order, many=True)
			order_serializer.is_valid()
			serializer = ClientSerializer(data=queryset, many=True)
			serializer.is_valid()
			context = {
				'client_obj': serializer.data,
				'clients_orders': order_serializer.data
			}
			return Response(context)
		serializer = ClientSerializer(data=queryset, many=True)
		serializer.is_valid()
		return Response(serializer.data)


class OrderVeiwSet(viewsets.ModelViewSet):
	queryset = Order.objects.select_related('client', 'driver').all()
	serializer_class = OrderSerializer
	filter_class = OrderFilterSet

	def create(self, request):
		driver_id = request.data.get('driver_id', 0)
		client_id = request.data.get('client_id', 0)
		if (client_id == 0 or client_id == "") or \
			(driver_id == 0 or driver_id == ""):
			context = {
				'error': 'driver_id or client_id is missing'
			}
			return Response(context)
		response = create_order(driver_id, client_id)
		if 'order_id' in response:
			queryset = Order.objects.filter(id=response['order_id']).select_related('client', 'driver')
			serializer = OrderSerializer(data=queryset, many=True)
			serializer.is_valid()
			context = {
				'order_obj': serializer.data
			}
			return Response(context)
		context = {
			'error': 'something went wrong'
		}
		return Response(context)

	def update(self, request, pk):
		status = request.data.get('status', 0)
		if (status == 0 or status == ""):
			context = {
				'error': 'status is missing'
			}
			return Response(context)
		response = order_status_update(pk, status)
		if 'status_exception' in response:
			context = {
				'error': 'accepted or finished status can not be canceled'
			}
			return Response(context)
		if 'order_id' in response:
			queryset = Order.objects.filter(id=response['order_id']).select_related('client', 'driver')
			serializer = OrderSerializer(data=queryset, many=True)
			serializer.is_valid()
			context = {
				'order_obj': serializer.data
			}
			return Response(context)
		context = {
			'error': 'something went wrong'
		}
		return Response(context)


class DriverVeiwSet(viewsets.ModelViewSet):
	queryset = Driver.objects.all()
	serializer_class = DriverSerializer