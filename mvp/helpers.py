from mvp.models import Client, Order, Driver


def create_order(driver_id, client_id):
	driver_id = int(driver_id)
	client_id = int(client_id)
	order = Order.objects.create(
				client_id=client_id,
				driver_id=driver_id
			)
	context = {
		'order_id': order.id
	}
	return context


def order_status_update(pk, status):
	order = Order.objects.filter(id=pk).first()
	if (order.status == 'accepted') or (order.status == 'finished') and\
		(status == 'canceled'):
		context = {
			'status_exception': 1
		}
		return context
	order.status = status
	order.save()
	context = {
		'order_id': order.id
	}
	return context