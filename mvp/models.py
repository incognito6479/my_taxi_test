from django.db import models
from mvp.choices import CAR_MODEL, ORDER_STATUS


class Client(models.Model):
	created = models.DateTimeField(auto_now_add=True)
	full_name = models.CharField(max_length=255, verbose_name="Client Full Name")
	phone = models.CharField(max_length=255, verbose_name="Client Phone number")

	def __str__(self):
		return f"{self.full_name} | {self.phone}"

	class Meta:
		ordering = ('id',)
		verbose_name = 'Client List'
		verbose_name_plural = 'Client List'


class Driver(models.Model):
	created = models.DateTimeField(auto_now_add=True)
	full_name = models.CharField(max_length=255, verbose_name="Driver Full Name")
	phone = models.CharField(max_length=255, verbose_name="Driver Phone number")
	car_model = models.CharField(max_length=255, verbose_name="Driver vehicle model",
								blank=True, null=True, choices=CAR_MODEL, default='best_car'
								)
	car_number = models.CharField(max_length=255, blank=True, null=True,
								verbose_name="Driver vehicle number"
								)

	def __str__(self):
		return f"{self.full_name} | {self.phone} | {self.car_model} | {self.car_number}"

	class Meta:
		ordering = ('id',)
		verbose_name = 'Driver List'
		verbose_name_plural = 'Driver List'


class Order(models.Model):
	created = models.DateTimeField(auto_now_add=True)
	driver = models.ForeignKey('mvp.Driver', on_delete=models.PROTECT,
	 						verbose_name="Driver", related_name='driver'
	 						)
	client = models.ForeignKey('mvp.Client', on_delete=models.PROTECT, 
							verbose_name="Client", related_name='client'
							)
	status = models.CharField(max_length=255, verbose_name="Order status",
							blank=True, null=True, choices=ORDER_STATUS, default='created'
							)

	def __str__(self):
		return f"{self.client.full_name} | {self.driver.full_name} | {self.status}"

	class Meta:
		ordering = ('id',)
		verbose_name = 'Order List'
		verbose_name_plural = 'Order List'