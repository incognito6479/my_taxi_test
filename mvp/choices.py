CAR_MODEL = (
	('matiz', 'Matiz'),
	('nexia', 'Nexia'),
	('spark', 'Spark'),
	('best_car', 'BMW'),
)


ORDER_STATUS = (
	('created', 'Created'),
	('accepted', 'Accepted'),
	('finished', 'Finished'),
	('canceled', 'Canceled')
)