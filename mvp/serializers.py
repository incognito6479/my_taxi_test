from rest_framework import serializers
from mvp.models import Client, Order, Driver


class ClientSerializer(serializers.ModelSerializer):
	class Meta:
		model = Client
		fields = 'id', 'full_name', 'phone'


class DriverSerializer(serializers.ModelSerializer):
	class Meta:
		model = Driver
		fields = 'id', 'full_name', 'phone', 'car_model', 'car_number'


class OrderSerializer(serializers.ModelSerializer):
	client_obj = ClientSerializer(many=False, read_only=True, source='client')
	driver_obj = DriverSerializer(many=False, read_only=True, source='driver')

	class Meta:
		model = Order
		fields = 'id', 'status', 'created', 'client_obj', 'driver_obj'