pyhton 3.8

To run the project first You have to create a virtual environment named .env and inside the .env folder create .env file and add theese
variables 

DATABASE_NAME='your_db_name'
DATABASE_USER='your_db_user_name'
DATABASE_PASSWORD='your_db_user_password'

GET - url/clients -> returns list of the clients
GET - url/drivers -> returns list of the drivers
GET - url/orders -> returns list of the orders

POST - url/orders/ data: { 'client_id':1, 'driver_id':1 } -> creates order with the given instances
PUT - url/orders/order_id data: { 'status': 'created/accepted/finished/canceled' } -> changes order status 

GET - url url/clients/?client_id=1&from=YYYY-MM-DD&to=YYYY-MM-DD -> returns all orders of the client in given range

and DON'T forget to install requirements.txt
